/*
 * arrays.cpp
 *
 *  Created on: 20-11-2013
 *      Author: Krystian
 */
#include <iostream>

int main()
{
	const int tab1[10] = { 1, 2, 3, 4, 5 }; // definicja 10 elementowej tablicy typu int

	for(int i = 0; i < 10; ++i)
		std::cout << tab1[i] << " ";
	std::cout << "\n";

	const int* ptr_item = tab1 + 2;  // tab1[2] === *(tab1 + 2)

	std::cout << "*ptr_item = " << *ptr_item << std::endl;

	//*ptr_item = -1;

	for(int i = 0; i < 10; ++i)
		std::cout << tab1[i] << " ";
	std::cout << "\n";

	// iteracja po tablicy przy pomocy wskaznika
	for(const int* it = tab1; it != tab1 + 10; ++it)
		std::cout << *it << " ";

	std::cout << std::endl;
}



