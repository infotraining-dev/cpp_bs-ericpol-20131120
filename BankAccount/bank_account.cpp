/*
 * bank_account.cpp
 *
 *  Created on: 22-11-2013
 *      Author: Krystian
 */

#include <iostream>
#include "bank_account.hpp"

using namespace std;

namespace Bank
{

//BankAccount::BankAccount()
//{
//	id_ = 0;
//	balance_ = 0;
//	owner_ = "Brak";
//}

// definicja skladowej statycznej
double BankAccount::interest_rate_ = 0.1;
unsigned int BankAccount::counter_;

BankAccount::BankAccount(const string& owner, double balance)
	:  balance_(balance), id_(++counter_), owner_(owner)
{
}

void BankAccount::withdraw(double amount)
{
	if (amount > balance_)
		throw std::logic_error("No cash exception");

	this->balance_ -= amount;   // this to wskaznik typu BankAccount* do obiektu
								// na rzecz kt�rego zostala wywolana metoda
}


void BankAccount::transfer_funds(BankAccount& target, double amount)
{
	withdraw(amount);
	target.deposit(amount);
}

//void BankAccount::print() const
//{
//	cout.precision(2);
//	cout << fixed << "BankAccount(id = " << id_ << ", balance = " << balance_
//		 << ", owner = " << owner_ << ")" << endl;
//}

ostream& operator <<(ostream& out, const BankAccount& account)
{
	out << fixed << "BankAccount(id = " << account.id_ << ", balance = " << account.balance()
			 << ", owner = " << account.owner() << ")";

	return out;
}

}



