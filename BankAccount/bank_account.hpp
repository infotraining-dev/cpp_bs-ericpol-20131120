/*
 * bank_account.hpp
 *
 *  Created on: 22-11-2013
 *      Author: Krystian
 */

#ifndef BANK_ACCOUNT_HPP_
#define BANK_ACCOUNT_HPP_

#include <string>
#include <stdexcept>

namespace Bank
{

class BankAccount
{
public:
	//BankAccount();
	BankAccount(const std::string& owner, double balance = 0.0); // konstruktor BankAccount
	virtual ~BankAccount() {}

	unsigned int id() const
	{
		return id_;
	}

	std::string owner() const
	{
		return owner_;
	}

	double balance() const
	{
		return balance_;
	}

	virtual void withdraw(double amount);

	virtual bool try_withdraw(double amount)
	{
		if (balance_ >= amount)
		{
			balance_ -= amount;
			return true;
		}

		return false;
	}

	void deposit(double amount)
	{
		balance_ += amount;
	}

	void transfer_funds(BankAccount& target, double amount);
	void pay_interest()
	{
		this->balance_ *= (1.0 + interest_rate_);
	}

	//void print() const;

	static double interest_rate()
	{
		return interest_rate_;
	}

	static void set_interest_rate(double interest_rate)
	{
		interest_rate_ = interest_rate;
	}

	friend std::ostream& operator <<(std::ostream& out, const BankAccount& account);
protected:
	double balance_;
private:
	const unsigned int id_;
	static double interest_rate_; // deklaracja skladowej statycznej
	static unsigned int counter_;
	std::string owner_;
};

std::ostream& operator <<(std::ostream& out, const BankAccount& account);




}

#endif /* BANK_ACCOUNT_HPP_ */
