/*
 * DebitAccount.cpp
 *
 *  Created on: 29-11-2013
 *      Author: Krystian
 */

#include "debit_account.hpp"

namespace Bank
{

DebitAccount::DebitAccount(const std::string& owner, double debit_limit, double balance)
	: BankAccount(owner, balance), debit_limit_(debit_limit)
{
}

void DebitAccount::withdraw(double amount)
{
	if (balance_ - amount < -debit_limit_)
		throw std::logic_error("No cash exception");

	balance_ -= amount;
}

bool DebitAccount::try_withdraw(double amount)
{
	if (balance_ - amount < -debit_limit_)
		return false;

	balance_ -= amount;
	return true;
}

DebitAccount::~DebitAccount()
{
	// TODO Auto-generated destructor stub
}

} /* namespace Bank */
