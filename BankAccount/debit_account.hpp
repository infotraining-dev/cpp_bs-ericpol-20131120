/*
 * DebitAccount.hpp
 *
 *  Created on: 29-11-2013
 *      Author: Krystian
 */

#ifndef DEBITACCOUNT_HPP_
#define DEBITACCOUNT_HPP_

#include "bank_account.hpp"

namespace Bank
{

class DebitAccount: public BankAccount
{
public:
	DebitAccount(const std::string& owner, double debit_limit, double balance = 0.0);
	void withdraw(double amount);
	bool try_withdraw(double amount);

	virtual ~DebitAccount();

private:
	double debit_limit_;
};

} /* namespace Bank */
#endif /* DEBITACCOUNT_HPP_ */
