/*
 * main.cpp
 *
 *  Created on: 22-11-2013
 *      Author: Krystian
 */
#include <iostream>
#include "bank_account.hpp"
#include "debit_account.hpp"

using namespace std;

int main()
{
	cout << "Stopa oprocentowanie: "
	     << Bank::BankAccount::interest_rate() << endl;


	Bank::BankAccount ba1("Kowalski");  // wywolanie konstruktora

	cout << ba1 << endl;

	ba1.deposit(1000.0);

	cout << ba1 << endl;

	ba1.pay_interest();

	cout << "Po wyplacie odsetek: ";

	cout << ba1 << endl;

	ba1.withdraw(500.0);

	cout << ba1 << endl;

	Bank::BankAccount::set_interest_rate(0.05);

	Bank::BankAccount ba2("Nowak", 3000.0);

	try
	{
		ba1.transfer_funds(ba2, 100000.99);
	}
	catch(const std::logic_error& e)
	{
		cout << "Nieudany transfer: " << e.what() << endl;
	}

	cout << ba1 << endl;
	cout << ba2 << endl;

	Bank::BankAccount ba3("Nijaki");  // dziala konstruktor domyslny
	ba3.deposit(2000);
	cout << ba3 << endl;

	ba3.pay_interest();

	cout << "Po wyplacie odsetek: ";
	cout << "ba3: " << ba3 << endl;

	cout << "Stopa oprocentowanie: "
		     << ba3.interest_rate() << endl;
//	 ZADANIE
//	 1. Do klasy BankAccount doda�:
//	 		+ metody odczytujace pola id_, balance_ oraz owner_
//	 2. Napisa� klase BankService:
//			+ przechowujaca wektor wskaznikow do kont bankowych
//	      + implementujaca operacje:
//				- dodanie konta
//				- usuniecie konta o zadanym id
//				- znalezienie konta o podanym id (zwraca wskaznik do konta)
//				- wplate na konto o podanym id
//	          - wyplate z konta o podanym id
//	          - przelew konta o id1 na konto o id2
//	          - wyswietlenie stanu wszystkich kont bankowych

	Bank::BankAccount* dba = new Bank::DebitAccount("Kowalewski", 2000.0);

	try
	{
		dba->withdraw(3000.0);
	}
	catch(const logic_error& e)
	{
		cout << e.what() << endl;
	}

	cout << "Konto po wyplacie: " << *dba << endl;

	delete dba;



}


