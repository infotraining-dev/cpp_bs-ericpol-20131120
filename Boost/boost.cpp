/*
 * boost.cpp
 *
 *  Created on: 29-11-2013
 *      Author: Krystian
 */

#include <boost/tokenizer.hpp>
#include <boost/foreach.hpp>
#include <string>
#include <iostream>
#include <vector>

using namespace std;

int main()
{
	string text = "Ala   ma    kota, Ola ma psa.   Bla-Bla-Bla!";

	boost::tokenizer<> tokens(text);

	vector<string> words;

	for(boost::tokenizer<>::iterator word = tokens.begin(); word != tokens.end(); ++word)
		words.push_back(*word);

//	for(vector<string>::iterator it = words.begin(); it != words.end(); ++it)
//		cout << *it << ",";
//	cout << endl;

	BOOST_FOREACH(const std::string& word, words)
	{
		cout << word << ",";
	}

	cout << endl;
}




