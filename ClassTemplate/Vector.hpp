/*
 * Vector.hpp
 *
 *  Created on: 29-11-2013
 *      Author: Krystian
 */

#ifndef VECTOR_HPP_
#define VECTOR_HPP_

#include <iostream>

template <typename T>
class Vector
{
	T* array_;
	size_t size_;
public:
	Vector(size_t size)
	{
		size_ = size;
		array_ = new T[size_];  // utworzenie tablicy na stercie

		// wyzerowanie tablicy
		for (unsigned int i = 0; i < size_; ++i)
			array_[i] = T();
	}

	Vector(const Vector& source)  // konstruktor kopiujacy
	{
		size_ = source.size_;
		array_ = new T[size_];

		// kopiowanie element�w
		for (size_t i = 0; i < size_; ++i)
			array_[i] = source.array_[i];
	}

	~Vector() // destruktor
	{
		delete[] array_;
	}

	size_t size()
	{
		return size_;
	}

	T& at(size_t index)
	{
		return array_[index];
	}

	const T& at(size_t index) const
	{
		return array_[index];
	}

	T& operator[](size_t index)
	{
		return array_[index];
	}

	const T& operator[](size_t index) const
	{
		return array_[index];
	}
};



#endif /* VECTOR_HPP_ */
