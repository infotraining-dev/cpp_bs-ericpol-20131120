/*
 * main.cpp
 *
 *  Created on: 29-11-2013
 *      Author: Krystian
 */

#include "Vector.hpp"
#include <string>

using namespace std;

int main()
{
	Vector<int> vec(10);

	for(size_t i = 0; i < vec.size(); ++i)
		cout << vec[i] << " ";
	cout << endl;

	Vector<string> words(3);

	words[0] = "Ala";
	words[1] = "ma";
	words[2] = "kota";

	for(size_t i = 0; i < words.size(); ++i)
			cout << words[i] << " ";
		cout << endl;

}


