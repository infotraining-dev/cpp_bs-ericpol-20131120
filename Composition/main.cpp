/*
 * main.cpp
 *
 *  Created on: 28-11-2013
 *      Author: Krystian
 */
#include <iostream>
#include <string>

using namespace std;

class Engine
{
public:
	Engine(unsigned short power, unsigned int capacity)
		: power_(power), capacity_(capacity), state_(false), rpm_(0)
	{
		cout << "Konstruktor silnika\n";
	}

	~Engine()
	{
		cout << "Destruktor silnika\n";
	}

	unsigned short power() const
	{
		return power_;
	}

	unsigned int capacity() const
	{
		return capacity_;
	}

	bool is_on() const
	{
		return state_;
	}

	unsigned int rpm() const
	{
		return rpm_;
	}

	void set_rpm(unsigned int rpm)
	{
		cout << "Setting rpm to " << rpm << endl;
		rpm_ = rpm;
	}

	void start()
	{
		cout << "Engine starts...\n";
		state_ = true;
		rpm_ = 800;
	}

	void stop()
	{
		cout << "Engine stops...\n";
		state_ = false;
		rpm_ = 0;
	}

private:
	unsigned short power_;
	unsigned int capacity_;
	bool state_;
	unsigned int rpm_;
};


class Car
{
	Engine engine_;
	unsigned int kmilage_;
	unsigned short year_;
public:
	Car(unsigned short power, unsigned int capacity, unsigned int year = 2013)
		: engine_(power, capacity), kmilage_(0), year_(year)
	{
		cout << "Konstruktor Car\n";
	}

	~Car()
	{
		cout << "Destruktor Car\n";
	}

	void drive(unsigned int km)
	{
		engine_.start();
		for(int i = 800; i < 2500; i+=200)
			engine_.set_rpm(i);

		kmilage_ += km;

		for(int i = 2500; i > 800; i-=200)
			engine_.set_rpm(i);

		engine_.stop();
	}

	void print_status()
	{
		cout << "Car: capacity=" << engine_.capacity() << " power=" << engine_.power()
		     << " km=" << kmilage_ << " year=" << year_ << endl;
	}
};

int main()
{
	Car beskid(700, 2500);

	beskid.print_status();

	beskid.drive(200);

	beskid.print_status();
}

