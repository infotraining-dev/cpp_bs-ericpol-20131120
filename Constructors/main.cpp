/*
 * main.cpp
 *
 *  Created on: 22-11-2013
 *      Author: Krystian
 */
#include <iostream>

using namespace std;

class Vector
{
	int* array_;
	size_t size_;
public:
	Vector(size_t size)
	{
		cout << "Konstruktor Vector: " << size << endl;

		size_ = size;
		array_ = new int[size_];  // utworzenie tablicy na stercie

		// wyzerowanie tablicy
		for (unsigned int i = 0; i < size_; ++i)
			array_[i] = 0;
	}

	Vector(const Vector& source)  // konstruktor kopiujacy
	{
		cout << "Konstruktor kopiujacy: " << source.size_ << endl;

		size_ = source.size_;
		array_ = new int[size_];

		// kopiowanie element�w
		for (size_t i = 0; i < size_; ++i)
			array_[i] = source.array_[i];
	}

	~Vector() // destruktor
	{
		cout << "Destruktor Vector: " << size_  << " address: "  << array_ << endl;

		delete[] array_;
	}

	size_t size()
	{
		return size_;
	}

	int& at(size_t index)
	{
		return array_[index];
	}

	const int& at(size_t index) const
	{
		return array_[index];
	}

	int& operator[](size_t index)
	{
		return array_[index];
	}

	const int& operator[](size_t index) const
	{
		return array_[index];
	}
};

int main()
{
	Vector vec_main(10);

	{
		Vector vec(3);

		for (size_t i = 0; i < vec.size(); ++i)
			cout << vec.at(i) << " ";
		cout << endl;

		vec.at(0) = 1;
		vec[1] = 2;
		vec.at(2) = 3;

		for (size_t i = 0; i < vec.size(); ++i)
			cout << vec.at(i) << " ";
		cout << endl;

		// kopiowanie wektora
		Vector vec_copy = vec;  // wywolanie konstruktor kopiujacy
		//Vector vec_copy(vec);

		vec_copy.at(0) = -10;

		for (size_t i = 0; i < vec.size(); ++i)
			cout << vec.at(i) << " ";
		cout << endl;

		Vector other_vec(20);

		vec = other_vec; // nie dziala konstruktor - dziala operator =
	}
}

