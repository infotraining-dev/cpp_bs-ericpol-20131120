/*
 * enums.cpp
 *
 *  Created on: 20-11-2013
 *      Author: Krystian
 */
#include <iostream>

// definicja typu
enum DayOfWeek { Mon = 1, Tue, Wed, Thd, Fri, Sat, Sun };

enum StateOfFile { Opened = 1, Closed = 2, ReadOnly = 4, WriteOnly = 8 };

void open_file(StateOfFile state)
{
	std::cout << "File opened in state: " << state << std::endl;
}

int main()
{
	DayOfWeek day = (DayOfWeek)4;
	int day_index = day;

	std::cout << "day = " << day << std::endl;
	std::cout << "day_index = " << day_index << std::endl;

	switch(day)
		{
		case Mon:
			std::cout << "Poniedzialek\n";
			break;
		case Tue:
			std::cout << "Wtorek\n";
			break;
		case Wed:
		case Thd:
			std::cout << "Sr lub Czw\n";
			break;
		case Fri:
			std::cout << "Piatek\n";
			break;
		case Sat:
		case Sun:
			std::cout << "Weekend\n";
			break;
		default:
			std::cout << "Nieprawidlowa wartosc dnia\n";
			break;
		}

	int state = Opened | WriteOnly;
	open_file((StateOfFile)state);
}


