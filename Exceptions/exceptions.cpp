/*
 * exceptions.cpp
 *
 *  Created on: 28-11-2013
 *      Author: Krystian
 */

#include <iostream>
#include <stdexcept>
#include <vector>

using namespace std;

class Number
{
	int number_;
public:
	Number(int n = 0) :
			number_(n)
	{
	}

	int value() const
	{
		return number_;
	}

	Number operator +(const Number& n) const  // operator + jako skladowa klasy
	{
		return Number(this->number_ + n.number_);
	}

	Number operator /(const Number& n) const
	{
		if (n.value() == 0)
			throw invalid_argument("Denominator is zero"); // zgloszenie bledu poprzez rzucenie wyjatku

		return Number(number_ / n.number_);
	}

	Number operator-() const
	{
		return Number(-number_);
	}

	bool operator ==(const Number& other) const
	{
		return number_ == other.number_;
	}

	bool operator !=(const Number& other) const
	{
		return !(*this == other);
	}

	Number& operator++()  // ++n
	{
		++number_;

		return *this;
	}

	const Number operator++(int) // n++
	{
		Number temp = *this;

		++number_;

		return temp;
	}

	friend istream& operator >>(istream& in, Number& n);
};

Number operator -(const Number& n1, const Number& n2)
{
	return Number(n1.value() - n2.value());
}

ostream& operator <<(ostream& out, const Number& n)
{
	out << n.value();

	return out;
}

istream& operator >>(istream& in, Number& n)
{
	int value;
	in >> value;

	n.number_ = value;

	return in;
}

Number calculate(const Number& n1, const Number& n2)
{
	cout << "Start calculation" << endl;

	Number result = n1 / n2;  // leci wyjatek

	cout << "End of calculation" << endl;

	return result;
}

int main()
{
	vector<Number> numbers;

	Number n1 = 4;
	Number n2 = 0;

	numbers.push_back(n1);
	numbers.push_back(n2);

	try
	{
		try
		{
			throw 13;
			Number result = calculate(n1, numbers.at(1));
			cout << "Result = " << result << endl;
		}
		catch (const invalid_argument& e)
		{
			cout << "Zlapalem wyjatek: " << e.what() << endl;
		}
		catch (const out_of_range& e)
		{
			cout << "Problem z indeksem: " << e.what() << endl;
		}
		catch (const exception& e)
		{
			cout << "Blad!" << endl;
		}
		catch (...)
		{
			cout << "Zlapalem ale nie wiem co!" << endl;

			throw;
		}
	} catch (...)
	{
		cout << "Obsluga wyjatkow z blokow catch" << endl;
	}

	cout << "Koniec programu" << endl;
}

