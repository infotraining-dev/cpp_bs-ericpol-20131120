/*
 * friends.cpp
 *
 *  Created on: 28-11-2013
 *      Author: Krystian
 */

#include <iostream>
#include <string>

using namespace std;

class Test;

class FriendWithTest
{
public:
	void print(const Test& t) const;
};

class Test
{
public:
	Test(int a, const string& text) : a_(a), text_(text)
	{}
private:
	int a_;
	string text_;

	friend void print(const Test& t);
	//friend void FriendWithTest::print(const Test& t) const;
	friend class FriendWithTest;
};

void FriendWithTest::print(const Test& t) const
{
	cout << "Printing from FriendWithTest - Test: " << t.a_ << " " << t.text_ << endl;
}

void print(const Test& t)
{
	cout << "Test: " << t.a_ << " " << t.text_ << endl;
}

int main()
{
	Test t1(1, "One");

	print(t1);

	FriendWithTest fwt;

	fwt.print(t1);
}



