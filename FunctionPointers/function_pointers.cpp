/*
 * function_pointers.cpp
 *
 *  Created on: 21-11-2013
 *      Author: Krystian
 */

#include <iostream>
#include <string>
#include <vector>
#include <cmath>

using namespace std;

int square(int x)
{
	return x * x;
}

double integrate(double a, double b, double (*function)(double))
{
	double result = 0.0;

	double dx = 0.01;
	for(double x = a; x < b; x += dx)
	{
		result += dx * function(x);
	}

	return result;
}

typedef void (*callback_type)();

void incoming_call(const std::string& phone_number, const std::vector<callback_type>& callbacks)
{
	cout << "incoming call: " << phone_number <<  endl;

	for(int i = 0; i < callbacks.size(); ++i)
		callbacks[i]();
}

void log()
{
	cout << "Saving to log file..." << endl;
}

void redirect_to_nsa()
{
	cout << "Redirecting to nsa..." << endl;
}

void redirect_to_abw()
{
	cout << "Redirecting to abw..." << endl;
}

int main()
{
	int (*fun_ptr)(int) = &square;

	// bezposrednie wywolanie square
	cout << "square(2) = " << square(2) << endl;

	// wywolanie przy pomocy wskaznika
	cout << "fun_ptr(2) = " << fun_ptr(2) << endl;

	cout << "Calka z sin(x)[0; pi/2]: " << integrate(0.0, M_PI_2, &sin) << endl;
	cout << "Calka z cos(x)[0; pi/2]: " << integrate(0.0, M_PI_2, &cos) << endl;

	// callback

	vector<callback_type> callbacks;
	callbacks.push_back(&log);
	callbacks.push_back(&redirect_to_nsa);
	callbacks.push_back(&redirect_to_abw);

	incoming_call("23 42343244", callbacks);
	incoming_call("54 54535343", callbacks);
}



