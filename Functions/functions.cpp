/*
 * functions.cpp
 *
 *  Created on: 21-11-2013
 *      Author: Krystian
 */
#include <iostream>
#include <string>
#include <iostream>
#include <cmath>
#include <tr1/array>

#include "mathematics.hpp"

using namespace std;

void print(const std::string& text)
{
	std::cout << "Printing text: " << text << std::endl;
}

int main()
{
	using namespace Math;

	int x = 10;
	int y = 20;

	cout << "x + y =  " << add(x, y) << std::endl;

	int wynik_dzielenia;
	int reszta;

	div(x, y, &wynik_dzielenia, &reszta);

	std::cout << "x / y = " << wynik_dzielenia << "; "
			<< "x % y = " << reszta << std::endl;

	div(x, y, wynik_dzielenia, reszta);

	const std::string str1 = "Text";
	print(str1);

	int numbers[5] = { 1, 2, 3, 4, 5 };
	std::tr1::array<int, 5> tab = { 1, 2, 3, 4, 5 };

	tab[0] = -1;

	std::cout << "sum: " << sum(numbers, 5) << std::endl;
	std::cout << "sum: " << sum(tab.data(), tab.size()) << std::endl;

	// dwuznaczne wywolanie pow
	// 1 - std::pow
	// 2 - Math::pow
	cout << Math::pow(2.0, 3.0) << endl;
	cout << std::pow(2.0, 3.0) << endl;
}
