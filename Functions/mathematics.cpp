/*
 * mathematics.cpp
 *
 *  Created on: 21-11-2013
 *      Author: Krystian
 */
#include "mathematics.hpp"
#include <iostream>

using namespace std;


int Math::add(int a, int b)
{
	return a + b;
}

namespace Math
{
	void div(int a, int b, int* r, int* d)
	{
		*r = a / b;
		*d = a % b;
	}

	void div(int a, int b, int& r, int& d)
	{
		r = a / b;
		d = a % b;
	}

	int sum(int tab[], unsigned int size)
	{
		int result = 0;

		for(int i = 0; i < size; ++i)
			result += tab[i];

		return result;
	}

	double pow(double x, double y)
	{
		cout << "Math::pow()\n";

		return 1.0;
	}
}
