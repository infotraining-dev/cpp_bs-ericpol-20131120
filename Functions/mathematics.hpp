/*
 * mathematics.hpp
 *
 *  Created on: 21-11-2013
 *      Author: Krystian
 */

#ifndef MATHEMATICS_HPP_
#define MATHEMATICS_HPP_

// using namespace std; // Blad - nie mozna uzywac w pliku h lub hpp

namespace Math
{
	int add(int a, int b);

	void div(int a, int b, int* r, int* d);

	void div(int a, int b, int& r, int& d);

	int sum(int tab[], unsigned int size);

	double pow(double x, double y);
}

#endif /* MATHEMATICS_HPP_ */
