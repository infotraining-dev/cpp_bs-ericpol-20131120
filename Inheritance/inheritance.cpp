/*
 * inheritance.cpp
 *
 *  Created on: 28-11-2013
 *      Author: Krystian
 */

#include <iostream>
#include <vector>
#include <stdexcept>
#include <string>
#include <typeinfo>

using namespace std;

struct RGB
{
	int r;
	int g;
	int b;

	RGB(int r = 0, int g = 0, int b = 0) : r(r), g(g), b(b)
	{}
};

ostream& operator <<(ostream& out, const RGB& color)
{
	out << "RGB(" << color.r << ", " << color.g << ", " << color.b << ")";

	return out;
}

class IShape  //
{
public:
	virtual void move(int dx, int dy) = 0;
	virtual void draw() const = 0;
	virtual ~IShape() {}
};

class Shape : public IShape
{
protected:
	int x_, y_;
public:
	Shape(int x = 0, int y = 0) : x_(x), y_(y)
	{
	}

	/*virtual*/ void move(int dx, int dy)
	{
		x_ += dx;
		y_ += dy;
	}

	virtual ~Shape() {}

	static Shape* create_shape(const std::string& id);
};

class Circle : 	public Shape // dziedziczenie po Shape
{
	int radius_;
public:
	Circle(int x = 0, int y = 0, int radius = 0) : Shape(x, y), radius_(radius)
	{
	}

	int radius() const
	{
		return radius_;
	}

	void set_radius(int radius)
	{
		radius_ = radius;
	}

	void draw() const
	{
		cout << "Drawing circle at (" << x_ << ", " << y_ << ") with r = " << radius_ << endl;
	}
};

class ColorCircle : public Circle
{
	RGB color_;
public:
	ColorCircle(int x = 0, int y = 0, int radius = 0, RGB color = RGB(255, 255, 255))
		: Circle(x, y, radius), color_(color)
	{
	}

	void draw() const
	{
		Circle::draw();  // wywolanie metody z klasy bazowej
		cout << "With color " << color_ << endl;
	}

	RGB color() const
	{
		return color_;
	}

	void set_color(const RGB& color)
	{
		color_ = color;
	}
};

class Rectangle : public Shape
{
	int width_, height_;
public:
	Rectangle(int x = 0, int y = 0, int w = 0, int h = 0)
		: Shape(x, y), width_(w), height_(h)
	{
	}

	void draw() const
	{
		cout << "Drawing a rectangle at (" << x_ << ", " << y_ << ")"
			 << " with width=" << width_ << " and height=" << height_ << endl;
	}

	int height() const
	{
		return height_;
	}

	void set_height(int height)
	{
		height_ = height;
	}

	int width() const
	{
		return width_;
	}

	void set_width(int width)
	{
		width_ = width;
	}
};

Shape* Shape::create_shape(const string& id)
{
	if (id == "Circle")
		return new Circle(100, 200, 300);
	else if (id == "Rectangle")
		return new Rectangle(300, 200, 500, 500);
	else if (id == "ColorCircle")
		return new ColorCircle(100, 300, 400, RGB(200, 100, 100));

	throw runtime_error("Bad shape id");
}

int main()
{
//	Shape s1(100, 200);
//	s1.draw();
//	s1.move(20, 40);
//	s1.draw();

	Circle c1(400, 500, 100);
	c1.draw();
	c1.move(40, 90);
	c1.draw();

	Rectangle r1(500, 200, 100, 400);
	r1.draw();
	r1.move(100, 200);
	r1.draw();

	cout << "\n";

	vector<Shape*> shapes;
	shapes.push_back(&c1);
	shapes.push_back(&r1);

	for(size_t i = 0; i < shapes.size(); ++i)
		shapes[i]->draw();

	cout << "\nRysunek:\n";

	vector<Shape*> drawing;

	drawing.push_back(new Rectangle(100, 200, 70, 30));
	drawing.push_back(new Circle(10, 20, 70));
	drawing.push_back(new Rectangle(200, 100, 170, 30));
	drawing.push_back(new ColorCircle(400, 200, 100, RGB(45, 33, 140)));

	for(size_t i = 0; i < drawing.size(); ++i)
		drawing[i]->draw();

	cout << "\nUstawienie promienia dla wszystkich okregow na 300:\n";

	for(size_t i = 0; i < drawing.size(); ++i)
	{
		if (Circle* ptr_circle = dynamic_cast<Circle*>(drawing[i]))
		{
			ptr_circle->set_radius(300);
		}
	}

	for(size_t i = 0; i < drawing.size(); ++i)
		drawing[i]->draw();

	for(size_t i = 0; i < drawing.size(); ++i)
		delete drawing[i];

	// Slicing

	cout << "\n\nSlicing:\n";

	ColorCircle cc(100, 200, 300, RGB(100, 100, 200));
	Circle c = cc; // slicing - trace w c kolor

	c.draw();

	cout << "\n\nSlicing:\n";

	// Upcasting - rzutowanie w g�re hierarchi dziedziczenia
	Circle* ptr_circle = new Circle(100, 500, 300);

	Shape* ptr_shape = ptr_circle; // dozwolone - niejawna konwersja Derived* na Base*
	ptr_shape->draw();
	delete ptr_shape;

	ptr_shape = new Rectangle(100, 600, 300, 200);
	ptr_shape->draw();	// skocz do tablicy funkcji dla obiektu wskazywanego, odczytaj adres funkcji i wywolaj
	delete ptr_shape;

	// Downcasting - rzutowanie w dol

	cout << "\n\nDowncasting:\n";

	ptr_circle = static_cast<Circle*>(Shape::create_shape("Circle"));

	ptr_circle->set_radius(100.0);
	ptr_circle->draw();

	delete ptr_circle;

	Shape* any_shape = Shape::create_shape("Rectangle");

	cout << "typeid.name() = " <<  typeid(any_shape).name() << endl;

	ptr_circle = dynamic_cast<Circle*>(any_shape);



	if (ptr_circle)
	{
		ptr_circle->set_radius(200);

		ptr_circle->draw();

		delete ptr_circle;
	}
	else
		cout << "Mam cie. any_shape nie jest typu Circle." << endl;






}
