/*
 * instructions.cpp
 *
 *  Created on: 20-11-2013
 *      Author: Krystian
 */
#include <iostream>

double x = 0.01; // zmienna globalna

int main()
{
	{ // blok kodu
 		int x = 10;
 		std::string local_txt = "Ala ma kota";

 		std::cout << local_txt << std::endl;
 		std::cout << "x = " << x << std::endl;
 		std::cout << "x = " << ::x << std::endl;
	}

	std::cout << "x = " << x << std::endl;

	// instrukcja warunkowa

	int n = 10;

	if ( n % 2 == 0 )
	{
		std::cout << n;
		std::cout << " jest parzyste\n";
	}
	else
		std::cout << n << " jest nieparzyste\n";

	unsigned char day = 7;

	switch(day)
	{
	case 1:
		std::cout << "Poniedzialek\n";
		break;
	case 2:
		std::cout << "Wtorek\n";
		break;
	case 3:
	case 4:
		std::cout << "Sr lub Czw\n";
		break;
	case 5:
		std::cout << "Piatek\n";
		break;
	case 6:
	case 7:
		std::cout << "Weekend\n";
		break;
	default:
		std::cout << "Nieprawidlowa wartosc dnia\n";
		break;
	}

	char znak = 65;

	switch(znak)
	{
	case 'A':
		std::cout << "Litera A\n";
		break;
	default:
		std::cout << "Inna litera\n";
	}
}




