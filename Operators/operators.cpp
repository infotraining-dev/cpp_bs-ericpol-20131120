#include <iostream>

int main()
{
	std::cout << std::dec << std::showbase;

	// operatory arytmetyczne
	int x = 10;
	int y = 3;

	std::cout << "x = " << x << "; y = " << y << std::endl;

	double div_result = 10/(double)3;

	std::cout << "x + y = " << (x + y) << std::endl;
	std::cout << "result = " << div_result << std::endl;

	int mod_result = 10 % 3;
	std::cout << "x % y = " << mod_result << std::endl;

	std::cout << "\n-----------------\n";

	// operatory inkrementacji
	x++;  // x = x + 1; post-inkrementacja
	y--;  // y = y - 1;
	++x; // pre-inkrementacja
	--y;

	std::cout << "x = " << x << "; y = " << y << std::endl;

	int incr_result = ++x + --y; // 13 + 0

	std::cout << "incr_result = " << incr_result << " "
			  << "x = " << x << "; y = " << y << std::endl;

	incr_result = x++ + y--; // 13 + 0

	std::cout << "incr_result = " << incr_result << " "
				  << "x = " << x << "; y = " << y << std::endl;

	std::cout << "\n-----------------\n";

	// operatory przypisania
	x = x + 5;
	x += 5;
	x *= 2;

	std::cout << "x = " << x << std::endl;

	// operatory bitowe
	unsigned int bx = 2;
	unsigned int by = 7;

	std::cout << "bx AND by = " << (bx & by) << std::endl;
	std::cout << "bx XOR by = " << (bx ^ by) << std::endl;
	std::cout << "NOT(bx OR by) = " << ~(bx | by) << std::endl;

	std::cout << "\n-----------------\n";

	// przesuniecie bitowe
	bx <<= 2; // bx = bx << 1;
	std::cout << "bx <<= 2 = " << bx << std::endl;

	bx >>= 1; // bx = bx << 1;
	std::cout << "bx >>= 1 = " << bx << std::endl;

	std::cout << "\n-----------------\n";

	// operatory porównania
	std::cout << "bx == by: " << std::boolalpha << (bx == by) << std::endl;
	std::cout << "bx != by: " << (bx != by) << std::endl;
	std::cout << "bx > by: " << (bx > by) << std::endl;
	std::cout << std::noboolalpha;
	std::cout << "bx <= by: " << (bx <= by) << std::endl;

	std::cout << "\n-----------------\n";

	// operatory logiczne (and, or, not)
	bool arg1 = true;
	bool arg2 = false;

	std::cout << "arg1 && arg2 = " << (arg1 && arg2) << std::endl; // and
	std::cout << "arg1 || arg2 = " << (arg1 || arg2) << std::endl; // or
	std::cout << "!arg1 = " << !arg1 << std::endl; // not

	double dx = 0.7;
	std::cout << "x nie jest w przedziale [0;1] = "
			  << ((dx <= 0.0) || (dx >= 1.0)) << std::endl;

	std::cout << "\n-----------------\n";

	// sizeof()
	std::cout << "sizof(int) = " << sizeof(int) << std::endl;
	std::cout << "sizeof(dx) = " << sizeof(dx) << std::endl;

	std::cout << "\n-----------------\n";

	// operator ? :
	x = 8;

	const char* txt_result = (x % 2 == 0) ? "parzysta" : "nieparzysta";
	std::cout << "x = " << x << ": " << txt_result << std::endl;
}


