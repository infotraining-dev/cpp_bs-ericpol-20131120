/*
 * overloading_operators.cpp
 *
 *  Created on: 28-11-2013
 *      Author: Krystian
 */

#include <iostream>

using namespace std;

class Number
{
	int number_;
public:
	Number(int n = 0) : number_(n)
	{
	}

	int value() const
	{
		return number_;
	}

	Number operator +(const Number& n) const  // operator + jako skladowa klasy
	{
		return Number(this->number_ + n.number_);
	}

	Number operator-() const
	{
		return Number(-number_);
	}

	bool operator ==(const Number& other) const
	{
		return number_ == other.number_;
	}

	bool operator !=(const Number& other) const
	{
		return !(*this == other);
	}

	Number& operator++()  // ++n
	{
		++number_;

		return *this;
	}

	const Number operator++(int) // n++
	{
		Number temp = *this;

		++number_;

		return temp;
	}

	friend istream& operator >>(istream& in, Number& n);
};

Number operator -(const Number& n1, const Number& n2)
{
	return Number(n1.value() - n2.value());
}

ostream& operator <<(ostream& out, const Number& n)
{
	out << n.value();

	return out;
}

istream& operator >>(istream& in, Number& n)
{
	int value;
	in >> value;

	n.number_ = value;

	return in;
}

int main()
{
	Number n1 = 1 ;  // niejawna konwersja przy pomocy konstruktora jednoargumentowego

	cout << "n1 = " << ++n1 << endl;

	Number n2;

 	cout << "Podaj n2: ";
 	cin >> n2;

 	cout << "n2 = " << n2++ << endl;

 	cout << "n2 = " << n2 << endl;

 	Number n3 = n1 + n2;

 	cout << "n3 = " << n3 << endl;

 	Number n4 = -n3;

 	cout << "n4 = " << n4 << endl;

 	if (n1 != n4)
 	{
 		cout << "n1 != n4" << endl;
 	}
 	else
 		cout << "n1 == n4" << endl;
}


