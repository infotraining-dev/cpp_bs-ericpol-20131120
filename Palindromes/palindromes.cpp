/*
 * palindromes.cpp
 *
 *  Created on: 21-11-2013
 *      Author: Krystian
 */

/*
 * palindromes.cpp
 *
 *  Created on: 21-11-2013
 *      Author: Krystian
 */

#include <iostream>
#include <string>
#include <cstring>

bool is_palindrome(const std::string& text)
{
	int i = 0;
	int j = text.length() - 1;

	while (i < j)
	{
		if (text[i] != text[j])
			return false;
		++i;
		--j;
	}

	return true;
}

bool is_palindrome(const char* text)
{
	int i = 0;
	int j = strlen(text) - 1;

	while (i < j)
	{
		if (text[i] != text[j])
			return false;

		++i;
		--j;
	}
//	int i, j;
//
//	for(i = 0, j = strlen(text)-1; i < j; ++i, --j)
//	{
//		if (text[i] != text[j])
//			return false;
//	}
//
//	return true;
}

bool is_palindrome(const char* first, const char* last)
{
	while(first < --last)
	{
		if (*first != *last)
			return false;

		++first;
	}
	return true;
}

int main()
{
//	std::string text;
//
//	std::cout << "Podaj tekst: ";
//	std::cin >> text;
//
//	if (is_palindrome(text.c_str()))
//		std::cout << text << " jest palindromem\n";
//	else
//		std::cout << text << " nie jest palindromem\n";
//
//	std::string text;


	char text[255];
	std::cout << "Podaj tekst: ";
	std::cin >> text;

	if (is_palindrome(&text[0], &text[0] + strlen(text)))
		std::cout << text << " jest palindromem\n";
	else
		std::cout << text << " nie jest palindromem\n";

}





