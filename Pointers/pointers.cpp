/*
 * pointers.cpp
 *
 *  Created on: 20-11-2013
 *      Author: Krystian
 */
#include <iostream>

int main()
{
	int x = 10;

	int* ptr = &x;

	std::cout << "x = " << x << std::endl;
	std::cout << "ptr = " << ptr << std::endl;
	std::cout << "*ptr = " << *ptr << std::endl;

	*ptr = 20; // zmiana wartosci wskazywanej przez ptr

	std::cout << "\nPo *ptr = 20:\n";

	std::cout << "x = " << x << std::endl;
	std::cout << "ptr = " << ptr << std::endl;
	std::cout << "*ptr = " << *ptr << std::endl;

	int y = 30;

	ptr = &y;

	std::cout << "\nPo ptr = &y:\n";

	std::cout << "x = " << x << std::endl;
	std::cout << "ptr = " << ptr << std::endl;
	std::cout << "*ptr = " << *ptr << std::endl;

	double pi = 3.14;

	std::cout << "\nptrd:\n";

	bool condition = false;

	double* ptrd = 0;

	if (condition)
	{
		ptrd = &pi;
	}

	std::cout << "ptrd = " << ptrd << std::endl;
	if (ptrd)
	{
		std::cout << "*ptrd = " << *ptrd << std::endl;
		*ptrd = 10.12;
		std::cout << "*ptrd = " << *ptrd << std::endl;
	}
}



