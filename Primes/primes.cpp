/*
 * primes.cpp
 *
 *  Created on: 20-11-2013
 *      Author: Krystian
 */

#include <iostream>
#include <cmath>
#include <vector>

//using std::cout;

using namespace std;

typedef vector<int> VectorPrimes;

bool is_prime(int number)
{
	for(int n = 2; n <= sqrt(number); ++n)
		if (number % n == 0)
		{
			return false;
		}

	return true;
}

// napisac funkcje, ktora zwraca vector<int> liczb pierwszych z zakresu [2, limit]
VectorPrimes get_primes(int limit)
{
	std::vector<int> primes;

	for(int number = 2; number <= limit; ++number)
		if (is_prime(number))
			primes.push_back(number);

	return primes;
}

void get_primes(int limit, std::vector<int>& primes)
{
	for(int number = 2; number <= limit; ++number)
		if (is_prime(number))
			primes.push_back(number);
}

int main()
{
	using namespace std;

	int upper_limit;
	cout << "Podaj zakres: ";
	cin >> upper_limit;

	cout << "Liczby pierwsze: ";

	//std::vector<int> primes = get_primes(upper_limit);
	vector<int> primes;
	get_primes(upper_limit, primes);

	typedef std::vector<int>::iterator PrimesIterator;

	for(PrimesIterator it = primes.begin(); it != primes.end(); ++it)
		cout << *it << " ";

	cout << std::endl;
}



