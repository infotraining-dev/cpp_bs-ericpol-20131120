/*
 * references.cpp
 *
 *  Created on: 21-11-2013
 *      Author: Krystian
 */

#include <iostream>
#include <string>

int main()
{
	//int& empty_ref; - referencja musi byc zainicjowana

	int x = 10;

	std::cout << "x = " << x << std::endl;

	int& rx = x; // rx jest referencja do x

	rx = 20;

	std::cout << "x = " << x << std::endl;

	x = 30;

	std::cout << "rx = " << rx << std::endl;

	const int& good_refence = 10;

	std::cout << "good_reference: " << good_refence << std::endl;

	std::string str1 = "text1";
	std::string& str1_ref = str1;

	std::cout << "sizeof(str1) = " << sizeof(str1) << std::endl;
	std::cout << "sizeof(str1_ref) = " << sizeof(str1_ref) << std::endl;
}


