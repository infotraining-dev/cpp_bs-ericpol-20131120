/*
 * structures.cpp
 *
 *  Created on: 22-11-2013
 *      Author: Krystian
 */

#include <iostream>
#include <vector>

using namespace std;

struct Person
{
	int id;
	const char* name;
	int age;
};

void init(Person& p, int id, const char* name, int age)
{
	p.id = id;
	p.name = name;
	p.age = age;
}

void print(const Person& p)
{
	cout << "Id: " << p.id << " Name: " << p.name << " Age: " << p.age << endl;
}

Person* create_person(int id, const char* name, int age)
{
	Person* p = new Person();  // utworzenie obiektu na sterci
	p->id = id;
	p->name = name;
	p->age = age;

	return p;
}

int main()
{
	Person p1;

	p1.id = 1;
	p1.name = "Kowalski";
	p1.age = 31;

	print(p1);


	Person p2;

	init(p2, 2, "Nowak", 23);

	print(p2);


	Person* ptr_to_person = &p2;

	(*ptr_to_person).age = 34;
	ptr_to_person->age = 35;  // operator -> : dostep do skladowej za pomoca wskaznika

	print(*ptr_to_person);


	cout << "\nvector<Person>:\n";

	vector<Person> vec;

	vec.push_back(p1); //
	vec.push_back(p2);

	vec.push_back(Person()); // nowego obiekt Person
	init(vec.back(), 3, "Nijaki", 34);

	Person anonim;
	vec.push_back(anonim);
	init(vec.back(), 4, "Kowalska", 22);

	vec[0].name = "Hoffander";

	for(unsigned int i = 0; i < vec.size(); ++i)
		print(vec[i]);

	cout << "p1: ";
	print(p1);

	cout << "\nvector<Person*>:\n";
	// wektor wskaznikow
	vector<Person*> vec_ptr;

	vec_ptr.push_back(&p1);
	vec_ptr.push_back(&p2);

	vec_ptr[0]->name = "Nijaka";

	for(unsigned int i = 0; i < vec_ptr.size(); ++i)
		print(*vec_ptr[i]);

	cout << "p1: ";
	print(p1);


	// sterta
	cout << "\nsterta:\n";

	Person* ptr1 = new Person; // utworzenie obiektu na stercie
	init(*ptr1, 1, "Nijaka", 32);

	print(*ptr1);

	delete ptr1; // usuniecie obiektu wskazywanego przez ptr1 z pamieci

	vector<Person*> people;
	people.push_back(create_person(34, "Kowalski", 22));
	people.push_back(create_person(35, "Kowalska", 62));
	people.push_back(create_person(36, "Kowal", 32));
	people.push_back(create_person(37, "Kowalewski", 52));

	for(unsigned int i = 0; i < people.size(); ++i)
		print(*people[i]);

	// wyczyszczenie pamieci po obiektach w wektorze
	for(unsigned int i = 0; i < people.size(); ++i)
		delete people[i];

	// operator new []
	Person* tab_person = new Person[3];  // utworzenie na stercie tablicy 3 elementow typu Person

	tab_person[0].id = 1;
	tab_person[0].name = "Kowal";
	tab_person[0].age = 44;
	init(tab_person[1], 3, "Nowak", 33);
	init(tab_person[2], 4, "Nowacki", 23);

	for(int i = 0; i < 3; ++i)
		print(tab_person[i]);

	delete [] tab_person; // zwolnienie tablicy tab_person
}











