//============================================================================
// Name        : CStrings.cpp
// Author      :
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <cstring>


int main()
{
	const char* txt1 = "Ala ma kota";  // char txt1[12] = { 'A', ..., '\0' };

	std::cout << "Pierwsza litera: " << txt1[0] << std::endl;
	std::cout << "Ostatnia litera: " << txt1[strlen(txt1)-1] << std::endl;

	// iteracja po txt1 przy pomocy wskaznika
	for(const char* ptr = txt1; *ptr != '\0'; ++ptr)
		std::cout << "'" << *ptr << "' ";
	std::cout << std::endl;

	// kopiowanie c-stringa
	const char* txt2 = txt1; // kopiowaniem wskaznikow

	std::cout << txt1 << " jest pod adresem " << (void*)txt1 << std::endl;
	std::cout << txt2 << " jest pod adresem " << (void*)txt2 << std::endl;

	const int SIZE = 255;
	char copy_of_txt1[SIZE]; // celowo za maly rozmiar tablicy znakow

	strcpy_s(copy_of_txt1, txt1); // buffer overrun!!!

	std::cout << copy_of_txt1 << " jest pod adresem " << (void*)copy_of_txt1 << std::endl;

	const char* text1 = "text";
	//text1[0] = 'T';

	std::cout << text1 << std::endl;

	// laczenie c-stringow
	const char* txt3 = " i Ola ma psa";

	const unsigned int size = strlen(txt1);
	char txt4[255];

	strcpy_s(txt4, txt1);
	strcat_s(txt4, txt3);

	std::cout << "txt4: " << txt4 << std::endl;


	system("PAUSE");
	return 0;
}
