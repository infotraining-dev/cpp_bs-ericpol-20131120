/*
 * strings.cpp
 *
 *  Created on: 21-11-2013
 *      Author: Krystian
 */

#include <iostream>
#include <string>

int main()
{
	std::string txt1 = "Ala ma kota";
	std::string txt2("Ola ma psa");

	std::cout << "txt1 = " << txt1 << std::endl;
	std::cout << "txt2 = " << txt2 << std::endl;

	// laczenie string�w przy pomocy operatora +
	std::string txt3 = txt1 + " " + txt2;
	std::cout << "txt3 = " << txt3 << std::endl;

	std::cout << "txt3 ma dlugosc: " << txt3.length() << std::endl;

	std::cout << "Pierwsza litera w txt1: " << txt1[0] << std::endl;
	std::cout << "Ostatnia litera w txt1: " << txt1[txt1.length()-1] << std::endl;

	// zmiana liter
	txt1[0] = 'E';
	std::cout << "txt1 = " << txt1 << std::endl;

	const char* cstr_txt1 = txt1.c_str();

	{
		std::string txt4 = "Problem";
		cstr_txt1 = txt4.c_str();
	}

	std::string txt5 = "Inny tekst";

	std::cout << "tekst" << cstr_txt1 << std::endl;

	system("PAUSE");
}