/*
 * templates.cpp
 *
 *  Created on: 29-11-2013
 *      Author: Krystian
 */

#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include <list>
#include <boost/lexical_cast.hpp>

using namespace std;

template<typename T>
const T& maximum(const T& a, const T& b)
{
	if (a < b)
		return b;
	return a;
}

template <typename T, typename Comparer>
const T& maximum(const T& a, const T& b, Comparer compare)
{
	if (compare(a, b))
		return a;
	return b;
}

class Person
{
public:
	Person(int id, const string& name)
		: id_(id), name_(name)
	{
	}

	int id() const
	{
		return id_;
	}

	void set_id(int id)
	{
		id_ = id;
	}

	const string& name() const
	{
		return name_;
	}

	void set_name(const string& name)
	{
		name_ = name;
	}

private:
	int id_;
	string name_;
};

bool operator >(const Person& pa, const Person& pb)
{
	return pa.id() > pb.id();
}

bool operator <(const Person& pa, const Person& pb)
{
	return pa.id() < pb.id();
}

ostream& operator <<(ostream& out, const Person& p)
{
	out << "Person(id = " << p.id() << ", name = " << p.name() << ")";

	return out;
}

// funkcja
bool compare_by_name(const Person& p1, const Person& p2)
{
	return p1.name() < p2.name();
}

// Funktor
class CompareByName
{
public:
	bool operator()(const Person& p1, const Person& p2) const
	{
		return p1.name() < p2.name();
	}
};

int main()
{
	cout << maximum(4, 7) << endl;
	cout << maximum(4.5, 3.1415) << endl;
	cout << maximum(static_cast<double>(4), 3.1415) << endl;
	cout << maximum<double>(4, 3.1415) << endl;

	string str1 = "Ala";
	string str2 = "Ola";

	cout << maximum(str1, str2) << endl;

	int number = 2;
	cout << maximum<string>(str1, boost::lexical_cast<string>(number)) << endl;

	cout << max(5, 6) << endl;

	Person p1(1, "Kowalski");
	Person p2(2, "Adamski");

	cout << maximum(p1, p2) << endl;
	cout << maximum(p1, p2, &compare_by_name) << endl;

	vector<string> words;
	words.push_back("Ola");
	words.push_back("Ala");
	words.push_back("Zenon");
	words.push_back("Marek");

	sort(words.begin(), words.end());

	cout << "\n\nWords:\n";
	for(vector<string>::iterator it = words.begin(); it != words.end(); ++it)
		cout << *it << "\n";

	vector<Person> team;
	team.push_back(p2);
	team.push_back(p1);
	team.push_back(Person(4, "Nijaki"));
	team.push_back(Person(3, "Kowal"));

	sort(team.begin(), team.end());

	cout << "\n\nTeam by id:\n";
	for(vector<Person>::iterator it = team.begin(); it != team.end(); ++it)
		cout << *it << "\n";

	//sort(team.begin(), team.end(), &compare_by_name);
	sort(team.begin(), team.end(), CompareByName());


	// C++11
	//sort(team.begin(), team.end(), [](const Person& p1, const Person& p2) { return p1.name() < p2.name(); });
	cout << "\n\nTeam by name:\n";
	for(vector<Person>::iterator it = team.begin(); it != team.end(); ++it)
		cout << *it << "\n";

	list<int> numbers;

	numbers.push_back(2);
	numbers.push_back(3);
	numbers.push_back(1);

	numbers.sort();
}

