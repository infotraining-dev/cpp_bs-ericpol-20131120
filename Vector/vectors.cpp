/*
 * vectors.cpp
 *
 *  Created on: 21-11-2013
 *      Author: Krystian
 */

#include <iostream>
#include <string>
#include <vector>

void print(const std::vector<int>& vec, const std::string& prefix = "vec")
{
	// iteracja po wektorze
	// iteracja po wektorze
	std::cout << prefix << ": ";
	for (unsigned int i = 0; i < vec.size(); ++i)
		std::cout << vec[i] << " ";
	std::cout << std::endl;
}

void legacy_print(int tab[], unsigned int size)
{
	for(int i = 0; i < size; ++i)
		std::cout << tab[i] << " ";
	std::cout << std::endl;
}

int main()
{
	std::vector<int> vec_int;  // pusty wektor elementów typu int

	std::cout << "vec_int size: " << vec_int.size() << std::endl;

	vec_int.push_back(1);
	vec_int.push_back(2);
	vec_int.push_back(3);

	vec_int.reserve(1000);
	for(int i = 1; i < 100; ++i)
		vec_int.push_back(i);

	//vec_int[5] = -10; // dzialanie niezdefiniowane
	//vec_int.at(5) = -10; // rzuca wyjatek std::out_of_range

	std::cout << "vec_int size: " << vec_int.size() << std::endl;

	// iteracja po wektorze
	// iteracja po wektorze
	print(vec_int, "vec_int");
	//vec_int.clear();

	std::cout << "vec_int size: " << vec_int.size() << std::endl;

	std::vector<int> copy_of_vec_int = vec_int;  // kopia wektora vec_int

	std::cout << "po kopii:\n";

	copy_of_vec_int.push_back(100);
	copy_of_vec_int[0] = -200;

	print(vec_int, "vec_int");
	print(copy_of_vec_int, "copy_of_vec_int");
	legacy_print(&vec_int[0], vec_int.size());

	// iteracja przy pomocy iteratorów
	for(std::vector<int>::iterator it = vec_int.begin(); it != vec_int.end(); ++it)
		std::cout << *it << " ";
	std::cout << std::endl;

}



