/*
 * main.cpp
 *
 *  Created on: 22-11-2013
 *      Author: Krystian
 */

#include <iostream>
#include <cmath>

using namespace std;

/*
�WICZENIE:

Napisa� klas� Vector2D b�d�c� implementacj� wektora w przestrzeni dwuwymiarowej. Klasa powinna zawiera�:
+ konstruktor dwuargumentowy z warto�ciami domy�lnymi r�wnymi zero
+ metod� length() zwracaj�c� d�ugo�� wektora
+ metody dost�powe: do odczytu wsp. x x() i do zapisu set_x() oraz do odczytu wsp. y y() i zapisu set_y()
+ metod� print() wy�wietlaj�c� wektor w formacie [1.5,0.33]
+ metod� add(const Vector2D& v) zwracaj�c� sum� dw�ch wektor�w: np. v3 = v1.add(v2)
+ metod� multiply(double a) zwracaj�c� wektor przemno�ony przez a
+ metod� multiply(const Vector2D& v) zwracaj�c� iloczyn skalarny dw�ch wektor�w
*/

class Vector2D
{
public:
	Vector2D(double x = 0.0, double y = 0.0);

	double x() const
	{
		return x_;
	}

	void set_x(double x)
	{
		x_ = x;
	}

	double y() const
	{
		return y_;
	}

	void set_y(double y)
	{
		y_ = y;
	}

	double length() const
	{
		return sqrt(x_ * x_ + y_ * y_);
	}

	Vector2D add(const Vector2D& v) const
	{
		return Vector2D(x_ + v.x_, y_ + v.y_);
	}

	Vector2D multiply(double a) const
	{
		return Vector2D(x_ * a, y_ * a);
	}

	double multiply(const Vector2D& v) const
	{
		return x_ * v.x_ + y_ * v.y_;
	}

	void print() const;
private:
	double x_, y_;
};

Vector2D::Vector2D(double x, double y) : x_(x), y_(y)
{
}

void Vector2D::print() const
{
	cout << "[" << x_ << ", " << y_ << "]\n";
}

void print_length(const Vector2D& v)
{
	cout << "Length of v: " << v.length() << endl;
}

int main()
{
	//U�ycie klasy Vector2D

	const Vector2D wersor_x(1.0);  // [1.0, 0.0]
	const Vector2D wersor_y(0, 1.0);

	Vector2D vec(5.0);  // Vector2D(5.0, 0.0)
	cout << "vec: ";
	vec.print();

	cout << "\nwersor x: ";
	wersor_x.print();

	Vector2D v1(1.0, -2.0);

	cout << "\nv1: ";
	v1.print();
	//cout << v1 << endl;
	cout << "v1.length() = " << v1.length() << endl;

	Vector2D v2(4.5, 1.5);
	cout << "\nv2: ";
	v2.print();

	cout << "\nv1 + v2 = ";
	Vector2D vr1 = v1.add(v2);
	vr1.print();

	cout << "\nv1 * 3.0 = ";
	Vector2D vr2 = v1.multiply(3.0);
	vr2.print();
}



